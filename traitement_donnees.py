#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import pickle


with open('liste_de_tag', 'rb') as fichier:
    mon_depickler = pickle.Unpickler(fichier)
    liste_de_tag = mon_depickler.load()


with open('dico_tag_numens', 'rb') as fichier:
    mon_depickler = pickle.Unpickler(fichier)
    dico = mon_depickler.load()

with open('liste_liste_film', 'rb') as fichier:
    mon_depickler = pickle.Unpickler(fichier)
    LL = mon_depickler.load()

##numero_tag permet d'voir le numéro d'ensemble d'un tag donné
def numero_tag(tag):
    return(dico[tag])


##ens_tag permet d'avoir l'ensemble des films liés aux tags
def ens_tag(tag):
    return(LL[numero_tag(tag)])

##ens_num permet d'avoir l'ensemble des films liés aux numéro
def ens_num(num):
    return(LL[num])    

##Donne l'intersection des ensebmles de films liés aux tags d'une liste (l_tag)
def intersect_tag(l_tag):
    e=set(ens_tag(l_tag[0]))
    for tag in l_tag[1:]:
        ei=set(ens_tag(tag))
        e=e&ei
    return(list(e))

##Donne l'intersection des ensebmles de films liés aux numéros d'une liste (l_num)
def intersect_num(l_num):
    e=set(ens_num(l_num[0]-1))
    for num in l_num[1:]:
        ei=set(ens_num(num-1))
        e=e&ei
    return(list(e))

# Fonction intermediaire pour le calcul des parties a k elements
def F(e,T): 
    L = []
    for x in T:
        L.append(x+[e])
    return L

n=len(liste_de_tag)

 # Calcul des parties a k elements dans {1...n}
def Partie(k,n):
    if k==0:
        return [[]]
    if k==1:
        return [[i] for i in range(1,n+1)]
    if k==n:
        return [[i for i in range(1,n+1)]]
    return Partie(k,n-1) + F(n,Partie(k-1,n-1))

#Donne la liste de toute les intersections à k éléments.
def inter_k_elem(k):
    liste_liste_num=Partie(k,len(liste_de_tag))
    L_e_k=[]
    for l_num in liste_liste_num:
        L_e_k.append(intersect_num(l_num))
    return(L_e_k)
   

#Donne la liste de toute les intersections possibles
def inter_tot():
    L_e=[[]]
    for k in range(1,len(liste_de_tag)+1):
        L_e.append(inter_k_elem(k))
    return(L_e)

print(inter_tot()[3])


#Affiche les films dans lesquels apparaissent Clooney, Pitt et Damon
#print(intersect(liste_de_tag[0:3]))
l=0
for i in LL:
    l+=len(i)
print(l)
#print(ens_tag("Brad Pitt")[0].movieID)
