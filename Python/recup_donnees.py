#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import pickle
from imdb import IMDb

ia = IMDb();
"""
print(ia.get_person_infoset())

persons = ia.search_person("George Clooney")
person_ID=persons[0].personID
print(person_ID)

clooney = ia.get_person(person_ID)
#ia.update(clooney, info='filmography')
print(clooney.keys())
print(clooney['actor'])



#movies = ia.get_keyword("freeman")
#keywords=ia.search_keyword("Scarface")
#print(movies)
#print(keywords)

"""
#liste_de_tag=["George Clooney","Brad Pitt", "Matt Damon", "Morgan Freeman", "Angelina Jolie", "Ryan Gosling", "Julia Roberts"]

#Liste des tags que l'on regarde
liste_de_tag=["George Clooney","Brad Pitt", "Matt Damon", "Leonardo Dicaprio","Ryan Gosling","Morgan Freeman"]

## dico est un dictionnaire dont les clés sont les tags et les valeurs, le numéro d'ensemble que l'on affecte aux tags
## LL est une liste d'ensemble de films liés aux tags

LL=[]
dico={}
num_tag=0

for tag in liste_de_tag:
    persons = ia.search_person(tag)
    person_ID=persons[0].personID
    print(person_ID)
    acteur=ia.get_person(person_ID)
    print(acteur)
    #acteurs=ia.search_person(tag)
    #acteur_ID=acteurs[0].personID
    #acteur=ia.get_person(acteur_ID)
    
    dico[tag]=num_tag
    LL.append(acteur['actor'])

    num_tag+=1

#print(dico)
#print(LL)

with open('liste_de_tag', 'wb') as fichier:
    mon_pickler = pickle.Pickler(fichier)
    mon_pickler.dump(liste_de_tag)


with open('dico_tag_numens', 'wb') as fichier:
    mon_pickler = pickle.Pickler(fichier)
    mon_pickler.dump(dico)

with open('liste_liste_film', 'wb') as fichier:
    mon_pickler = pickle.Pickler(fichier)
    mon_pickler.dump(LL)
