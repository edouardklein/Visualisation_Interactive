###############################################################################
#                 PARTIE INTERACTIVE (tres sommaire)
###############################################################################
# Description des objets importants :
#       Input (recupere de Venn.py et lexico.py) :
#           - la liste des n polygones
#           - la liste "tags" des n tags
#           - la liste "movies" du contenu de 2^n s
#       Output :
#           - un visuel du diagramme avec matplotlib.pyplot
#           - quand on passe la souris a un endroit, l'interieur des polygones
#             a l'interieur desquels on se trouve se met en surbrillance. Les 
#             tags et les contenus de l'intersection de ces tags sont egalement
#             indiques. 
###############################################################################

from Venn import*
from mpl_toolkits.axes_grid.anchored_artists import AnchoredText
import re

t1 = time()
Layers,Polygons = drawVenn(1)
t2 = time()
print("Temps de conception des polygones: "+str(t2-t1))

fig, ax = plt.subplots()
plt.axis('equal')

for k in range(n):
    Polygons[k].Draw("black")

t3 = time()

print("Temps de trace des polygones: "+str(t3-t2))

at1 = AnchoredText("", prop=dict(size=15), frameon=True, loc=2)
at2 = AnchoredText("", prop=dict(size=8), frameon=True, loc=3)

all_movies = set([])
for k in range(n):
    all_movies = all_movies | set(movies[0][k])

def getCurrentColors(event):
    global at1,at2
    x,y = event.xdata, event.ydata    
    at1.set_visible(False)
    at2.set_visible(False)
    if x!=None and y!=None:
        for k in range(n):
            X = [Polygons[k].sommets[i][0] for i in range(Polygons[k].size)]
            Y = [Polygons[k].sommets[i][1] for i in range(Polygons[k].size)]
            plt.fill(X,Y,"white")
            plt.fill(X,Y,colors[k],alpha=0)
            
        current_tags = ""
        current_movies = all_movies
        
    
        for k in range(n):
            X = [Polygons[k].sommets[i][0] for i in range(Polygons[k].size)]
            Y = [Polygons[k].sommets[i][1] for i in range(Polygons[k].size)]
            if Polygons[k].PinPoly((x,y)):
                plt.fill(X,Y,colors[k],alpha=2/float(n))
                current_tags += (tags[k]+", ")
                current_movies = set(current_movies) & set(movies[0][k])
        current_tags = current_tags[:-2]
        if current_movies == all_movies:
            current_movies = []
        current_movies = list(current_movies)
        if len(current_movies)>100:
            current_movies = current_movies[:100] + [" (" + str(len(current_movies)-100) + " others)"]
        current_movies = ", ".join(current_movies)
        current_movies = re.sub("(.{257})", "\\1\n", current_movies, 0, re.DOTALL)
        
        at1 = AnchoredText(current_tags, prop=dict(size=15), frameon=True, loc=2)
        at2 = AnchoredText(current_movies, prop=dict(size=9), 
                           bbox_to_anchor=[0.2,0.1], frameon=True, loc=3)
        if len(current_movies)<257:
            at2 = AnchoredText(current_movies, prop=dict(size=9), 
                               bbox_to_anchor=[0.5*1360*(1-len(current_movies)/257.),0.1], frameon=True, loc=3)
        
        at1.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
        at2.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
        ax.add_artist(at1)
        ax.add_artist(at2)
        
        
cid = fig.canvas.mpl_connect('motion_notify_event', getCurrentColors)
