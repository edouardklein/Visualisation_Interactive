from imdb import IMDb

ia = IMDb();
"""
print(ia.get_person_infoset())

persons = ia.search_person("George Clooney")
person_ID=persons[0].personID
print(person_ID)

clooney = ia.get_person(person_ID)
#ia.update(clooney, info='filmography')
print(clooney.keys())
print(clooney['actor'])



#movies = ia.get_keyword("freeman")
#keywords=ia.search_keyword("Scarface")
#print(movies)
#print(keywords)

"""
#liste_de_tag=["George Clooney","Brad Pitt", "Matt Damon", "Morgan Freeman", "Angelina Jolie", "Ryan Gosling", "Julia Roberts"]

#Liste des tags que l'on regarde
liste_de_tag=["George Clooney","Brad Pitt", "Matt Damon", "Leonardo Dicaprio"]

## dico est un dictionnaire dont les clés sont les tags et les valeurs, le numéro d'ensemble que l'on affecte aux tags
## LL est une liste d'ensemble de films liés aux tags

LL=[]
dico={}
num_tag=0

for tag in liste_de_tag:
    persons = ia.search_person(tag)
    person_ID=persons[0].personID
    print(person_ID)
    acteur=ia.get_person(person_ID)
    print(acteur)
    #acteurs=ia.search_person(tag)
    #acteur_ID=acteurs[0].personID
    #acteur=ia.get_person(acteur_ID)
    
    dico[tag]=num_tag
    LL.append(acteur['actor'])

    num_tag+=1

#print(dico)
#print(LL)


##numero_tag permet d'voir le numéro d'ensemble d'un tag donné
def numero_tag(tag):
    return(dico[tag])


##ens_tag permet d'avoir l'ensemble des films liés aux tags
def ens_tag(tag):
    return(LL[numero_tag(tag)])


##Donne l'intersection des ensebmles de films liés aux tags d'une liste (l_tag)
def intersect(l_tag):
    e=set(ens_tag(l_tag[0]))
    for tag in l_tag[1:]:
        ei=set(ens_tag(tag))
        e=e&ei
    return(list(e))


#Affiche les films dans lesquels apparaissent Clooney, Pitt et Damon
print(intersect(liste_de_tag[0:3]))

